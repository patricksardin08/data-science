import numpy as np
import pandas as pd
from yarl import URL
from statistics import mean
from pickle import dump, load
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import OrdinalEncoder

i = 0
def filter_action(df):
    all_actions = list()
    
    global i
    for element in df["name"]:
        
        url = URL(element)
        if url.query_string != '' and  url.query_string in element:
            element = element[:element.index(url.query_string)]
        elif url.query_string not in element:
            element = element[:element.index('html')+len('html')]
        #last element in the url
        action = element.split('/')[-1]
        precedent_action = element.split('/')[-2]
        prec_prec_action = element.split('/')[-3] if len(element.split('/'))>2 else ''
        
        if '.html' in action:
            action = action[:action.index('.html')]
            all_actions.append(action)
            
        elif prec_prec_action == 'order_id':
            action = prec_prec_action + precedent_action
            all_actions.append(action)
            
        elif prec_prec_action == 'reward_product':
            action = prec_prec_action
            all_actions.append(action)
            
        elif prec_prec_action == 'rewardpoints':
            action = prec_prec_action + 'and' + precedent_action
            all_actions.append(action)
            
        elif prec_prec_action == 'id':
            action = 'edit'+ prec_prec_action + precedent_action
            all_actions.append(action)
            
        elif precedent_action == 'id':
            action =  prec_prec_action + precedent_action + action
            all_actions.append(action)
        
        elif action == '' or precedent_action in ['account', 'login']:
            action = precedent_action
            if action == 'www.josera.de' or action == 'www.josera.de//':
                action = 'start'
            all_actions.append(action)
        else:
            all_actions.append(action)
        
        i=i+1
    return all_actions

#clean the action and joit it as one word
def join_action(action):
    symb=['-', '=', '_', '.', '?', '&', ',', '#', ' ' ]
    for s in symb:
        action = action.split(s)
        action = ''.join(action)
    return action


def rewrite_column(df, column_name):
    df[column_name] = list(map(join_action, df[column_name]))
    return df


def group_by_userid(df):
    user_id = list()
    actions = list()
    
    for UserID in df['UserID']:
        if UserID not in user_id:
            actions.append(df[df['UserID'] == UserID]['Aktionen'].tolist())
            user_id.append(UserID)
            
    return actions




def write_urls_as_list(matomo_urls):

    all_actions = list()
    i = 0
    for element in matomo_urls:
        
        url = URL(element)
        if url.query_string != '' and  url.query_string in element:
            element = element[:element.index(url.query_string)]
        elif url.query_string not in element:
            element = element[:element.index('html')+len('html')]
        #last element in the url
        action = element.split('/')[-1]
        precedent_action = element.split('/')[-2]
        prec_prec_action = element.split('/')[-3] if len(element.split('/'))>2 else ''
        
        if '.html' in action:
            action = action[:action.index('.html')]
            all_actions.append(action)
            
        elif prec_prec_action == 'order_id':
            action = prec_prec_action + precedent_action
            all_actions.append(action)
            
        elif prec_prec_action == 'reward_product':
            action = prec_prec_action
            all_actions.append(action)
            
        elif prec_prec_action == 'rewardpoints':
            action = prec_prec_action + 'and' + precedent_action
            all_actions.append(action)
            
        elif prec_prec_action == 'id':
            action = 'edit'+ prec_prec_action + precedent_action
            all_actions.append(action)
            
        elif precedent_action == 'id':
            action =  prec_prec_action + precedent_action + action
            all_actions.append(action)
        
        elif action == '' or precedent_action in ['account', 'login']:
            action = precedent_action
            if action == 'www.josera.de' or action == 'www.josera.de//':
                action = 'start'
            all_actions.append(action)
        else:
            all_actions.append(action)
        
        i=i+1
    return all_actions

def rewrite_actions(list_actions):
    new_list = list(map(join_action, list_actions))
    return new_list

def min_sec(x):
    return x.strftime('%M:%S')

def unique(x):
    return x[0]

def get_mean_time(datetime_list):
    
    list_sec = list()
    
    if len(datetime_list) == 1:
        list_sec.append(0)
    else:
        for i in range(len(datetime_list)):
            if i+1 == len(datetime_list):
                break
            if (datetime_list[i].hour == datetime_list[i+1].hour) and(datetime_list[i].minute == datetime_list[i+1].minute):
                if i+1< len(datetime_list):
                    list_sec.append(abs(datetime_list[i].second - datetime_list[i+1].second))
                
            elif datetime_list[i].hour == datetime_list[i+1].hour and datetime_list[i].minute != datetime_list[i+1].minute:
                if i+1< len(datetime_list):
                    list_sec.append(abs(datetime_list[i].second - datetime_list[i+1].second))
                    
            elif datetime_list[i].hour != datetime_list[i+1].hour and datetime_list[i].minute != datetime_list[i+1].minute:
                if i+1< len(datetime_list):
                    sub_hour = abs(datetime_list[i].hour - datetime_list[i+1].hour)
                    sub_min = abs(datetime_list[i].minute - datetime_list[i+1].minute)*60 
                    sub_sec = abs(datetime_list[i].second - datetime_list[i+1].second)
                    if sub_hour>1:
                        list_sec.append(1800)
                    else:
                        list_sec.append(sub_min+sub_sec)
    return mean(list_sec)

def count_(x):
    return len(x)
