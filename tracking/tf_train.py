
import argparse
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Embedding, Activation, Dropout, TimeDistributed, RepeatVector
from tensorflow.keras.layers import  Conv1D, GlobalMaxPooling1D, GlobalAveragePooling1D
from tensorflow.keras.preprocessing.sequence import pad_sequences
import tensorflow as tf
#import matplotlib.pyplot as plt
import json
import os
import numpy as np
import pandas as pd


def parse_args():
    parser = argparse.ArgumentParser()

    # hyperparameters, die später eingestellt werden müssen, werden hier als command-line arguments addiert
    parser.add_argument('--epochs', type=int, default=60)
    parser.add_argument('--batch-size', type=int, default=50)
    #parser.add_argument('--learning-rate', type=float, default=0.1)
    
    #input data und model Ordner
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    #parser.add_argument('--model-dir', type=str)
    parser.add_argument('--training', type=str, default=os.environ['SM_CHANNEL_TRAINING'])
    #parser.add_argument('--training', type=str, default='data')
    #parser.add_argument('--validation', type=str, default=os.environ['SM_CHANNEL_VALIDATION'])
    #parser.add_argument('--test', type=str, default=os.environ['SM_CHANNEL_TEST'])
    args, _ = parser.parse_known_args()
    return args
def buid_model(time_tensor, os_tensor, browser_tensor, action_input_tensor, action_output_tensor, max_seq_len):
    
    num_unique_os = 12+1
    num_unique_browser = 43+1
    num_unique_actions = 2616+1
   
    
    os_emb_size = 25
    browser_emb_size = 50
    actions_emb_size = 2616
    
    #numeric Input 
    numerical_input = tf.keras.Input(shape=(1,), name='numeric_input') 
    
    
    #categorical Input
    os_input = tf.keras.Input(shape=(1,), name='os_input')
    browser_input = tf.keras.Input(shape=(1,), name='browser_input')
    action_input= tf.keras.Input(shape=(max_seq_len,), name='action_input')
    
    emb_os = Embedding(num_unique_os, os_emb_size)(os_input)  
    emb_browser = Embedding(num_unique_browser, browser_emb_size)(browser_input)
    emb_actions = Embedding(num_unique_actions, actions_emb_size)(action_input)
    
    actions_repr = LSTM(120)(emb_actions)
    actions_repr = Dropout(0.5)(actions_repr)
    
    
    os_repr = tf.squeeze(emb_os, axis=1)
    browser_repr = tf.squeeze(emb_browser, axis=1)
    
    activity_repr = tf.keras.layers.Concatenate()([os_repr, browser_repr, actions_repr, numerical_input])
    
    x = Dense(120, activation='relu')(activity_repr)
    x = Dropout(0.5)(x)
    next_n_actions = Dense(num_unique_actions-1, activation='softmax')(x)
    
    model = tf.keras.Model(inputs=[numerical_input, os_input, browser_input, action_input], outputs = [next_n_actions])
    model.summary()
    
  
    model.compile('nadam', 'categorical_crossentropy', metrics=['accuracy']) 
    
    history = model.fit({'numeric_input': time_tensor,
                         'os_input': os_tensor,
                         'browser_input': browser_tensor,
                         'action_input': action_input_tensor}, 
                          action_output_tensor, 
                          batch_size=770, 
                          epochs=70,
                          validation_split=0.2,
                          verbose=1)
    
    acc = history.history['accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1, len(acc) + 1)
    
    #plotting training and validation loss
#     plt.plot(epochs, loss, label='Training loss')
#     plt.plot(epochs, val_loss, label='validation loss')
#     plt.title('Training and validation loss')
#     plt.xlabel('Epochs')
#     plt.ylabel('Loss')
#     plt.legend()
#     plt.show()
    
#     #plotting training and validation accuracy
#     plt.clf()

#     val_acc = history.history['val_accuracy']
#     plt.plot(epochs, acc, label='Training acc')
#     plt.plot(epochs, val_acc, label='Validation acc')
#     plt.title('Training and validation accuracy')
#     plt.xlabel('Epochs')
#     plt.ylabel('acc')
#     plt.legend()
#     plt.show()
    
    model.save('LSTM_Model_clicsPrediction_0104.h5')
    
    return model

if __name__ == "__main__":
    
    args = parse_args()
    epochs     = args.epochs
    batch_size = args.batch_size
    model_dir  = args.model_dir
    training_dir   = args.training
    
    input_train =np.load(os.path.join(training_dir, 'training.npz'))['train_input']
    action_output_tensor =np.load(os.path.join(training_dir, 'training.npz'))['train_output']
    
    time_tensor = input_train[:,0].astype(np.float64)
    os_tensor = input_train[:,1].astype(np.int64)
    browser_tensor = input_train[:,2].astype(np.int64)
    action_input_tensor = input_train[:,3:].astype(np.int64)
    
    print ("shape time_tenso:", time_tensor.shape)
    print ("shape os_tensor:", os_tensor.shape)
    print ("shape browser_tensor:", browser_tensor.shape)
    print ("shape input_num:", action_input_tensor.shape)
    print ("shape target:", action_output_tensor.shape)
    
    max_seq_len = 495
    
    model = buid_model(time_tensor, os_tensor, browser_tensor, action_input_tensor, action_output_tensor, max_seq_len)
    
    os.makedirs("./model", exist_ok = True)
    save_path = "./model"
    model.save(os.path.join(save_path, "tracking_model.h5"))
    model.save(os.path.join(save_path, "tracking_model"))
    
    print('Saving Model in Tensorflow Format')
    model.save(os.path.join(model_dir, '0000001'),"tracking_model.h5") 
    
