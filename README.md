This project is about predicting user behavior on e-commerce websites.

Current relevant information is for us:

1.  The operating system
2.  the browser used
3.  The actions of the user represented as urls
4.  The time the user spends on the url

That means here we have three categorical variables and one numerical variable.

**Steps:**

1. Download the dataset as pandas Dataframe
2. Bring all fonctions for data preparation together
3. Clean the dataset with the fonctions above and sort them into differents input variables and the output as array
4. Merge the arrays and then store them in s3 as unique channel
5. Creation of the Entry point (train.py): This is where the environment variables are set. The training data is fetched from s3 and processed to input variables and the model is built.
6. Train with TensorFlow on the Notebook instance/(aka 'local mode)/ Train with Tf GPU instance
7. Testing the Keras model after trained it by calling train.py script, as if it were outside Sagemaker


