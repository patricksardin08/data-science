import json
import requests


def handler(data, context):
    """Handle request.
    Args:
        data (obj): the request data
        context (Context): an object containing request and configuration details
    Returns:
        (bytes, string): data to return to client, (optional) response content type
    """
    processed_input = _process_input(data, context)
    response = requests.post(context.rest_uri, data=processed_input)
    return _process_output(response, context)


def _process_input(data, context):
    if context.request_content_type == 'application/json':
        # pass through json (assumes it's correctly formed)
        #decoded_data = data.read().decode('utf-8')
     # Converts the JSON object to an array that meets the model signature.
        input_array = input_to_arrays(data)
        return input_array 
       
    raise ValueError('{{"error": "unsupported content type {}"}}'.format(context.request_content_type or "unknown"))


def _process_output(data, context):
    if data.status_code != 200:
        raise ValueError(data.content.decode('utf-8'))

    response_content_type = context.accept_header
    prediction = data.content
    return prediction, response_content_type


        
def input_to_arrays(request):
    time_pro_event = request['time_pro_event']
    os_name = str(request['os_name'])
    browser_name = str(request['browser_name'])
    user_urls = request['user_urls']
    
    #load label encoder objects
    #import pdb; pdb.set_trace()
    le_betriebsystem = pickle.load(open('Labelencoder/le_betriebsystem.pkl', 'rb'))
    le_browser = pickle.load(open('Labelencoder/le_browser.pkl', 'rb'))
    le_action = pickle.load(open('Labelencoder/le_actionen.pkl', 'rb'))

    max_seq_len = 55
    cat_1, cat_2, cat_3, num =input_data_builder(time_pro_event, os_name, browser_name, user_urls, 
                                                le_betriebsystem, le_browser, le_action,  max_seq_len, URL)
    return (num, cat_1, cat_2, cat_3)




def input_data_builder(time_pro_event, os_name, browser_name, user_urls, 
                      le_betriebsystem, le_browser, le_actionen, max_seq_len, URL):
    
    list_action = write_urls_as_list(user_urls, URL)
    user_events = rewrite_actions(list_action)
    
    cat_1 = le_betriebsystem.transform([os_name]).reshape(1, 1).astype(np.int32)
    cat_2 = le_browser.transform([browser_name]).reshape(1,1).astype(np.int32)
    cat_3 = le_actionen.transform(user_events)
    cat_3 = pad_sequences([cat_3], maxlen=max_seq_len, truncating='pre').astype(np.int32)
    
    num = np.array(sum(time_pro_event)/len(time_pro_event)).reshape(1,1)
    
    return cat_1, cat_2, cat_3, num



def rewrite_column(df, column_name):
    df[column_name] = list(map(join_action, df[column_name]))
    return df

def rewrite_actions(list_actions):
    new_list = list(map(join_action, list_actions))
    return new_list

#clean the action and joit it as one word
def join_action(action):
    symb=['-', '=', '_', '.', '?', '&', ',', '#', ' ' ]
    for s in symb:
        action = action.split(s)
        action = ''.join(action)
    return action

def write_urls_as_list(matomo_urls, URL):
    URL = URL
    all_actions = list()
    i = 0
    for element in matomo_urls:
        
        url = URL(element)
        if url.query_string != '':
            element = element[:element.index(url.query_string)-1]
        
        if '/' in element:
            #last element in the url
            action = element.split('/')[-1]
            precedent_action = element.split('/')[-2]
            prec_prec_action = element.split('/')[-3] if len(element.split('/'))>2 else ''
            
            if '.html' in action:
                action = action[:action.index('.html')]
                all_actions.append(action)
                
            elif prec_prec_action == 'order_id':
                action = prec_prec_action + precedent_action
                all_actions.append(action)
                
            elif prec_prec_action == 'rewardpoints':
                action = prec_prec_action + 'and' + precedent_action
                all_actions.append(action)
                
            elif prec_prec_action == 'id':
                action = 'edit'+ prec_prec_action + precedent_action
                all_actions.append(action)
                
            elif precedent_action == 'id':
                action =  prec_prec_action + precedent_action + action
                all_actions.append(action)
            
            elif action == '' or precedent_action in ['account', 'login']:
                action = precedent_action
                if action == 'www.josera.de':
                    action = 'start'
                all_actions.append(action)
            else:
                all_actions.append(action)
        else:
            all_actions.append(element)
        
        i=i+1
    return all_actions
   
