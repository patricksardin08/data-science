
import argparse
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Embedding, Activation, Dropout, TimeDistributed, RepeatVector
from tensorflow.keras.layers import  Conv1D, GlobalMaxPooling1D, GlobalAveragePooling1D
from tensorflow.keras.preprocessing.sequence import pad_sequences
import tensorflow as tf
import json
import os
import numpy as np
import pandas as pd

if __name__ == "__main__":
        
    parser = argparse.ArgumentParser()

    # hyperparameters, die später eingestellt werden müssen, werden hier als command-line arguments addiert
    parser.add_argument('--epochs', type=int, default=60)
    parser.add_argument('--batch-size', type=int, default=50)
    #parser.add_argument('--learning-rate', type=float, default=0.1)
    
    #input data und model Ordner
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    #parser.add_argument('--model-dir', type=str)
    parser.add_argument('--training', type=str, default=os.environ['SM_CHANNEL_TRAINING'])
    #parser.add_argument('--training', type=str, default='data')
    #parser.add_argument('--validation', type=str, default=os.environ['SM_CHANNEL_VALIDATION'])
    #parser.add_argument('--test', type=str, default=os.environ['SM_CHANNEL_TEST'])
     
    args, _ = parser.parse_known_args()
    
    epochs     = args.epochs
    #lr         = args.learning_rate
    batch_size = args.batch_size
    model_dir  = args.model_dir
    training_dir   = args.training
    #validation_dir   = args.validation
    
    input_train =np.load(os.path.join(training_dir, 'training.npz'))['train_input']
    target =np.load(os.path.join(training_dir, 'training.npz'))['train_output']
    
    print ("input_train shape:", input_train.shape)
    print ("target shape:", target.shape)
    
    input_cat1 = input_train[:,0].astype(np.int32)
    input_cat2 = input_train[:,1].astype(np.int32)
    input_cat3 = input_train[:,3:].astype(np.int32)
    input_num = input_train[:,2].astype(np.float32)
    
    print ("shape input_cat1:", input_cat1.shape)
    print ("shape input_cat2:", input_cat2.shape)
    print ("shape input_cat3:", input_cat3.shape)
    print ("shape input_num:", input_num.shape)
    print ("shape target:", target.shape)
    
    n_steps = 2                        # number of timesteps in each sample
    num_unique_os = 5                  #len(le_betriebsystem.classes_)+1
    num_unique_browser = 10            #len(le_browser.classes_)+1
    num_unique_actions = 210           #len(le_actionen.classes_)+1
    
    os_emb_size = 32
    browser_emb_size = 32
    actions_emb_size = 64
    max_seq_len = 55
    
    numerical_input = tf.keras.Input(shape=(1,), name='numeric_input')
    #categorical Input
    os_input = tf.keras.Input(shape=(1,), name='os_input')
    browser_input = tf.keras.Input(shape=(1,), name='browser_input')
    action_input= tf.keras.Input(shape=(max_seq_len,), name='action_input')
    
    emb_os = tf.keras.layers.Embedding(num_unique_os, os_emb_size)(os_input) 
    emb_browser = tf.keras.layers.Embedding(num_unique_browser, browser_emb_size)(browser_input)
    emb_actions = tf.keras.layers.Embedding(num_unique_actions, actions_emb_size)(action_input)
    
    actions_repr = tf.keras.layers.LSTM(120, return_sequences=True)(emb_actions)
    actions_repr = tf.keras.layers.LSTM(60)(emb_actions)
    
    emb_os = tf.squeeze(emb_os, axis=1)
    emb_browser = tf.squeeze(emb_browser, axis=1)
    
    activity_repr = tf.keras.layers.Concatenate()([emb_os, emb_browser, actions_repr, numerical_input])

    x = tf.keras.layers.RepeatVector(n_steps)(activity_repr)
    x = tf.keras.layers.LSTM(120, return_sequences=True)(x) 
    next_n_actions = tf.keras.layers.Dense(num_unique_actions-1, activation='softmax')(x)
    
    model = tf.keras.Model(inputs=[numerical_input, os_input, browser_input, action_input], outputs = next_n_actions)
    model.compile('adam', 'categorical_crossentropy', metrics=['accuracy'])
    
    history = model.fit({'numeric_input': input_num,
            'os_input': input_cat1,
            'browser_input': input_cat2,
            'action_input': input_cat3}, target, batch_size=50, epochs=150)
    
    os.makedirs("./model", exist_ok = True)
    save_path = "./model"
    model.save(os.path.join(save_path, "user_tracking_model.h5"))
    model.save(os.path.join(save_path, "user_tracking_model"))
    
    print('Saving Model in Tensorflow Format')
    
    
    model.save(os.path.join(model_dir, '01'),"user_tracking_model.h5") 
 
