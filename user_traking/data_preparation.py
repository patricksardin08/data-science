# transform url into action
def filter_action(df):
    all_actions = list()
    i = 0
    for element in df['Aktionen']:
        
        url = URL(element)
        if url.query_string != '':
            element = element[:element.index(url.query_string)-1]
        
        #last element in the url
        action = element.split('/')[-1]
        precedent_action = element.split('/')[-2]
        prec_prec_action = element.split('/')[-3] if len(element.split('/'))>2 else ''
        
        if '.html' in action:
            action = action[:action.index('.html')]
            all_actions.append(action)
            
        elif prec_prec_action == 'order_id':
            action = prec_prec_action + precedent_action
            all_actions.append(action)
            
        elif prec_prec_action == 'reward_product':
            action = prec_prec_action
            all_actions.append(action)
            
        elif prec_prec_action == 'rewardpoints':
            action = prec_prec_action + 'and' + precedent_action
            all_actions.append(action)
            
        elif prec_prec_action == 'id':
            action = 'edit'+ prec_prec_action + precedent_action
            all_actions.append(action)
            
        elif precedent_action == 'id':
            action =  prec_prec_action + precedent_action + action
            all_actions.append(action)
        
        elif action == '' or precedent_action in ['account', 'login']:
            action = precedent_action
            if action == 'www.josera.de' or action == 'www.josera.de//':
                action = 'start'
            all_actions.append(action)
        else:
            all_actions.append(action)
        
        i=i+1
    return all_actions

#clean the action and joit it as one word
def join_action(action):
    action = action.split('-')
    action = ''.join(action)
    action = action.split('=')
    action = ''.join(action)
    action = action.split('_')
    action = ''.join(action)
    action = action.split('.')
    action = ''.join(action)
    action = action.split('?')
    action = ''.join(action)
    action = action.split('&')
    action = ''.join(action)
    action = action.split(',')
    action = ''.join(action)
    action = action.split('#')
    action = ''.join(action)
    action = action.split(' ')
    return ''.join(action)


def rewrite_column(df, column_name):
    df[column_name] = list(map(join_action, df[column_name]))
    return df


def group_by_userid(df):
    mean_zeit = list()
    user_id = list()
    actions = list()
    betriebsystem = list()
    browser = list()
    
    for UserID in df['UserID']:
        if UserID not in user_id:
            mean_zeit.append(df[df['UserID'] == UserID]['Zeit der Aktion(sec)'].mean())
            actions.append(df[df['UserID'] == UserID]['Aktionen'].tolist())
            betriebsystem.append(df[df['UserID'] == UserID]['Betriebsystem'].tolist()[0])
            browser.append(df[df['UserID'] == UserID]['Browser'].tolist()[0])
            user_id.append(UserID)
            
    return betriebsystem, browser, actions, mean_zeit

def write_urls_as_list(matomo_urls, URL):
    URL = URL
    all_actions = list()
    i = 0
    for element in matomo_urls:
        
        url = URL(element)
        if url.query_string != '':
            element = element[:element.index(url.query_string)-1]
        
        if '/' in element:
            #last element in the url
            action = element.split('/')[-1]
            precedent_action = element.split('/')[-2]
            prec_prec_action = element.split('/')[-3] if len(element.split('/'))>2 else ''
            
            if '.html' in action:
                action = action[:action.index('.html')]
                all_actions.append(action)
                
            elif prec_prec_action == 'order_id':
                action = prec_prec_action + precedent_action
                all_actions.append(action)
                
            elif prec_prec_action == 'rewardpoints':
                action = prec_prec_action + 'and' + precedent_action
                all_actions.append(action)
                
            elif prec_prec_action == 'id':
                action = 'edit'+ prec_prec_action + precedent_action
                all_actions.append(action)
                
            elif precedent_action == 'id':
                action =  prec_prec_action + precedent_action + action
                all_actions.append(action)
            
            elif action == '' or precedent_action in ['account', 'login']:
                action = precedent_action
                if action == 'www.josera.de':
                    action = 'start'
                all_actions.append(action)
            else:
                all_actions.append(action)
        else:
            all_actions.append(element)
        
        i=i+1
    return all_actions

def rewrite_actions(list_actions):
    new_list = list(map(join_action, list_actions))
    return new_list
    


def generate_step_seq(model, tokenizer_actions, betriebsystem_dic, browser_dic,actions_dic,
                      max_seq_len, name_betriebsystem, name_browser, user_urls, time_pro_event, num_step):
    
    list_action = write_urls_as_list(user_urls)
    user_events = rewrite_actions(list_action)
    
    steps = list()
    
    for _ in range(num_step):
         
        i = 0
        actual_events = tokenizer_actions.texts_to_sequences([user_events])[0]
        actual_events = pad_sequences([actual_events], maxlen=max_seq_len, truncating='pre')
        
        betriebsystem_value = np.array(betriebsystem_dic[name_betriebsystem]).reshape(1,1)
        browser_value = np.array(browser_dic[name_browser]).reshape(1,1)
        mean_time = np.array(sum(time_pro_event)/len(time_pro_event)).reshape(1,1)
        
        y_pred = model.predict([mean_time, betriebsystem_value, browser_value, actual_events])
        y_predicted = y_pred.argmax(axis=-1)
        
        predicted_step = ''
        for word, index in actions_dic.items():
            if index == y_predicted:
                predicted_step = word
                break
            
        user_events.append(predicted_step)
        steps.append(predicted_step)
        
        i = i+1
    
    return ' '.join(steps)

def test_data_builder(name_betriebsystem, name_browser, user_urls, le_betriebsystem, 
                      le_browser, le_actionen, time, steps, max_seq_len, URL):
    
    list_action = write_urls_as_list(user_urls, URL)
    user_events = rewrite_actions(list_action)
    
    cat_1 = le_betriebsystem.transform([name_betriebsystem]).reshape(1, 1).astype(np.int32)
    cat_2 = le_browser.transform([name_browser]).reshape(1,1).astype(np.int32)
    cat_3 = le_actionen.transform(user_events)
    cat_3 = pad_sequences([cat_3], maxlen=max_seq_len, truncating='pre').astype(np.int32)
    
    num = np.array(sum(time_pro_event)/len(time_pro_event)).reshape(1,1)
    
    return cat_1, cat_2, cat_3, num
